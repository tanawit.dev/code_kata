require_relative '../balance_statements'

describe '' do
  it 'Should return "Buy: 162600.0 Sell: 0" when statement is "GOOG 300 542.0 B"' do
    assert_equal balance_statements("GOOG 300 542.0 B"),
                                    "Buy: 162600 Sell: 0"
  end

  it 'Should return "Buy: 29499 Sell: 0"
      when statement is "ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B, OWW 1000 11.623 B, OGG 20 580.1 B"' do
    assert_equal balance_statements("ZNGA 1300 2.66 B, CLH15.NYM 50 56.32 B, OWW 1000 11.623 B, OGG 20 580.1 B"),
                                    "Buy: 29499 Sell: 0"
  end

  it 'Should return "Buy: 169850 Sell: 116000; Badly formed 1: CSCO 250.0 29 B ;"
      when statement is "GOOG 300 542.0 B, AAPL 50 145.0 B, CSCO 250.0 29 B, GOOG 200 580.0 S"' do
    assert_equal balance_statements("GOOG 300 542.0 B, AAPL 50 145.0 B, CSCO 250.0 29 B, GOOG 200 580.0 S"),
                                    "Buy: 169850 Sell: 116000; Badly formed 1: CSCO 250.0 29 B ;"
  end
end
