require_relative '../keep_order'

describe '' do
  it 'Should return 4 when arry is [1, 2, 3, 4, 7] and val is 5' do
    assert_equal keep_order([1, 2, 3, 4, 7], 5), 4
  end

  it 'Should return 0 when list is [1, 2, 3, 4, 7] and val is 0' do
    assert_equal keep_order([1, 2, 3, 4, 7], 0), 0
  end

  it 'Should return 2 when list is [1, 1, 2, 2, 2] and val is 2' do
    assert_equal keep_order([1, 1, 2, 2, 2], 2), 2
  end

  it 'Should return 0 when list is [1, 2, 3, 4] and val is -1' do
    assert_equal keep_order([1, 2, 3, 4], -1), 0
  end

  it 'Should return 1 when list is [1, 2, 3, 4] and val is 2' do
    assert_equal keep_order([1, 2, 3, 4], 2), 1
  end

  it 'Should return 0 when list is [] and val is 2' do
    assert_equal keep_order([], 2), 0
  end
end
