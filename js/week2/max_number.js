const maxNumber = (number) => {
  let numberArr = String(number).split("");
  return +numberArr.sort((a, b) => +b - +a).join("");
}

module.exports = maxNumber
