const balanceStatements = (str = "") => {
  const orderCommands = str.split(","),
    regex = /^([A-z\S]+)\s(\d\S+)\s(\d*[.]\d+)\s[B-S]/i;

  const result = orderCommands.reduce((acc, cur) => {
    let currentCommand = cur.trim();
    if (regex.test(currentCommand)) {
      const [quote, quantity, price, status] = [...currentCommand.split(" ")];
      acc[status] += +quantity * +price;
    } else {
      acc.invalidFormats.push(`Badly formed ${acc.invalidFormats.length + 1}: ${currentCommand} ;`);
    }
    return acc;
  }, { B: 0, S: 0, invalidFormats: []});

  return `Buy: ${result.B} Sell: ${result.S}${result.invalidFormats.length ? "; " +result.invalidFormats.join("") : ""}`
}

module.exports = balanceStatements