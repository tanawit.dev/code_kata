const maxNumber = require('../max_number')

test('Should return 321 when number is 213', () => {
  expect(maxNumber(213)).toEqual(321)
})

test('Should return 97_632 when number is 63_729', () => {
  expect(maxNumber(63729)).toEqual(97632)
})

test('Should return 977_665 when number is 566_797', () => {
  expect(maxNumber(566797)).toEqual(977665)
})

test('Should return 8_754_321 when number is 2_478_315', () => {
  expect(maxNumber(2478315)).toEqual(8754321)
})

test('Should return 76_432 when number is 34_672', () => {
  expect(maxNumber(34672)).toEqual(76432)
})
