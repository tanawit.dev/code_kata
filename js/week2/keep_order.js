const keepOrder = (ary, val) => {
  return ary.reduce((acc, cur, index, arr) => {
    if (cur < val) {
      acc = cur;
    } else if (cur === val) {
      if (arr[index + 1] == val) {
        acc = cur;
      }
    }
    return acc;
  }, 0);
}

module.exports = keepOrder