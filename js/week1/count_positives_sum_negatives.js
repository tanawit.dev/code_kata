const countPositivesSumNegatives = (list) => {
  return list.reduce((acc, cur) => {
    if (cur > 0) acc[0]++;
    else acc[1] += cur;
    return acc;
  }, [0, 0]);
}

module.exports = countPositivesSumNegatives
