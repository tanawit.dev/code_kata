const reverseOrder = (list) => {
  /* modify itself (similar to reverse build-in)
  const length = Math.floor(list.length / 2);
  for (let index = 1; index <= length; index++) {
    const temp = list[index - 1];
    list[index - 1] = list[list.length - index];
    list[list.length - index] = temp;
  }
  return list;
  */
  const temp = [...list];
  return list.map(x => temp.pop());
}

module.exports = reverseOrder
