const setAlarm = require('../set_alarm')

test('Should return false when employed is true and vacation is true', () => {
  expect(setAlarm(true, true)).toBe(false)
})

test('Should return false when employed is false and vacation is true', () => {
  expect(setAlarm(false, true)).toBe(false)
})

test('Should return false when employed is false and vacation is false', () => {
  expect(setAlarm(false, false)).toBe(false)
})

test('Should return true when employed is true and vacation is false', () => {
  expect(setAlarm(true, false)).toBe(true)
})
