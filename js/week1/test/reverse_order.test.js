const reverseOrder = require('../reverse_order')

test('Should return [5, 1, 2, 7, 9] when list is [9, 7, 2, 1, 5]', () => {
  expect(reverseOrder([9, 7, 2, 1, 5])).toEqual([5, 1, 2, 7, 9])
})

test('Should return [3, 1, 4, 5, 6, 8] when list is [8, 6, 5, 4, 1, 3]', () => {
  expect(reverseOrder([8, 6, 5, 4, 1, 3])).toEqual([3, 1, 4, 5, 6, 8])
})
